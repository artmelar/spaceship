import org.junit.jupiter.api.*;
import java.util.ArrayList;

public class TestWithLibrary {

    CommandCenter commandCenter = new CommandCenter();
    static ArrayList<Spaceship> testList = new ArrayList<>();

    @DisplayName("Returns that the powerful ship does not exist")
    @Test
    void getMostPowerfulShip_returnNull() {
        testList.add(new Spaceship("Life", 0, 0, 0));
        testList.add(new Spaceship("Leaf", 0, 0,0));
        testList.add(new Spaceship("Lime", 0, 0,0));
        testList.add(new Spaceship("Lol", 0, 0,0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        Assertions.assertNull(result);
    }

    @DisplayName("Returns the first most powerful ship")
    @Test
    void getMostPowerfulShip_returnFirstMostPowerfulShip() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50,0));
        testList.add(new Spaceship("Lime", 100, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        Assertions.assertEquals("Life", result.getName());
    }

    @DisplayName("Returns the most powerful ship on the list")
    @Test
    void getMostPowerfulShip_returnMostPowerfulShip() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50,0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        Assertions.assertEquals(100, result.getFirePower());

    }

    @DisplayName("Returns the ship with the specified name")
    @Test
    void getShipByName_returnShipFound() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50,0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        Spaceship result = commandCenter.getShipByName(testList, "Life");
        Assertions.assertEquals("Life", result.getName());
    }

    @DisplayName("Returns that a ship with this name does not exist")
    @Test
    void getShipByName_returnNull() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50,0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        Spaceship result = commandCenter.getShipByName(testList, "Lie))");
        Assertions.assertNull(result);
    }

    @DisplayName("Returns a list of ships with the required cargo space")
    @Test
    void getAllShipsWithEnoughCargoSpace_returnList() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50,0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        ArrayList <Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 10);
        Assertions.assertEquals(3, result.size());
    }

    @DisplayName("Returns an empty list of ships with the required cargo space")
    @Test
    void getAllShipsWithEnoughCargoSpace_returnEmptyList() {
        //
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50,0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 125);
        Assertions.assertEquals(true, result.isEmpty());

    }

    @DisplayName("Returns a list of civilian ships")
    @Test
    void getAllCivilianShips_returnList() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50, 0));
        testList.add(new Spaceship("Lime", 0, 25,0));
        testList.add(new Spaceship("Lol", 0, 0,0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testList);
        Assertions.assertEquals(2, result.size());

    }

    @DisplayName("Returns an empty list of civilian ships")
    @Test
    void getAllCivilianShips_returnEmptyList() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50, 0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testList);
        Assertions.assertEquals(true, result.isEmpty());

    }

    @AfterEach
    void clearArrayList() {
        testList.clear();
    }
}
