import java.util.ArrayList;

public class CommandCenterTest {

    CommandCenter commandCenter = new CommandCenter();
    ArrayList <Spaceship> testList = new ArrayList<>();

    public static void main(String[] args) {

        CommandCenterTest commandCenterTest = new CommandCenterTest();
        double point = 0;

        boolean testMostPowerfulShip_1 = commandCenterTest.getMostPowerfulShip_mostPowerfulShipExists_returnNull();
        if (testMostPowerfulShip_1) {
            System.out.println("1. testMostPowerfulShip_1 passed successfully");
            point += 0.5;
        } else {
            System.out.println("1. Test failed(");
        }

        boolean testMostPowerfulShip_2 = commandCenterTest.getMostPowerfulShip_mostPowerfulShip_returnFirst();
        if (testMostPowerfulShip_2) {
            System.out.println("2. testMostPowerfulShip_2 passed successfully");
            point += 0.5;
        } else {
            System.out.println("2. Test failed(");
        }

        boolean testMostPowerfulShip_3 = commandCenterTest.getMostPowerfulShip_mostPowerfulShip_returnMostPowerfulShip();
        if (testMostPowerfulShip_3) {
            System.out.println("3. testMostPowerfulShip_3 passed successfully");
            point += 0.5;
        } else {
            System.out.println("3. Test failed(");
        }

        boolean testName_1 = commandCenterTest.getShipByName_shipFound_returnShipWithSpecifiedName();
        if (testName_1) {
            System.out.println("4. testName_1 passed successfully");
            point += 0.5;
        } else {
            System.out.println("4. Test failed(");
        }

        boolean testName_2 = commandCenterTest.getShipByName_shipNotExist_returnNull();
        if (testName_2) {
            System.out.println("5. testName_2 passed successfully");
            point += 0.5;
        }else {
            System.out.println("5. Test failed(");
        }

        boolean testEnoughCargo_1 = commandCenterTest.getAllShipsWithEnoughCargoSpace_returnList();
        if (testEnoughCargo_1) {
            System.out.println("6. testEnoughCargo_1 passed successfully");
            point += 0.5;
        }else {
            System.out.println("6. Test failed(");
        }

        boolean testEnoughCargo_2 = commandCenterTest.getAllShipsWithEnoughCargoSpace_returnEmptyList();
        if (testEnoughCargo_2) {
            System.out.println("7. testEnoughCargo_2 passed successfully");
            point += 0.5;
        }else {
            System.out.println("7. Test failed(");
        }

        boolean testCivilianShips_1 = commandCenterTest.getAllCivilianShips_returnList();
        if (testCivilianShips_1) {
            System.out.println("8. testCivilianShips_1 passed successfully");
            point += 0.5;
        }else {
            System.out.println("8. Test failed(");
        }

        boolean testCivilianShips_2 = commandCenterTest.getAllCivilianShips_returnEmptyList();
        if (testCivilianShips_2) {
            System.out.println("9. testCivilianShips_2 passed successfully");
            point += 0.5;
        }else {
            System.out.println("9. Test failed(");
        }

        System.out.println("Points: " + point + " / 4.5");

    }

    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnNull() {
        testList.add(new Spaceship("Life", 0, 0, 0));
        testList.add(new Spaceship("Leaf", 0, 0, 0));
        testList.add(new Spaceship("Lime", 0, 0,0));
        testList.add(new Spaceship("Lol", 0, 0,0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        testList.clear();
        return result == null;
    }

    private boolean getMostPowerfulShip_mostPowerfulShip_returnFirst() {
        testList.add(new Spaceship("Life", 100, 0, 0));
        testList.add(new Spaceship("Leaf", 99, 0,0));
        testList.add(new Spaceship("Lime", 100, 0,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        testList.clear();
        return result.getFirePower() == 100 && result.getName().equals("Life");
    }

    private boolean getMostPowerfulShip_mostPowerfulShip_returnMostPowerfulShip() {
        testList.add(new Spaceship("Life", 100, 0, 0));
        testList.add(new Spaceship("Leaf", 99, 0,0));
        testList.add(new Spaceship("Lime", 98, 0,0));
        testList.add(new Spaceship("Lol", 97, 0,0));
        Spaceship result = commandCenter.getMostPowerfulShip(testList);
        testList.clear();
        return result.getFirePower() == 100;
    }

    private boolean getShipByName_shipFound_returnShipWithSpecifiedName() {
        testList.add(new Spaceship("Life", 100, 0, 0));
        testList.add(new Spaceship("Leaf", 99, 0, 0));
        testList.add(new Spaceship("Lime", 98, 0,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        Spaceship result = commandCenter.getShipByName(testList, "Life");
        testList.clear();
        return result.getName().equals("Life");
    }

    private boolean getShipByName_shipNotExist_returnNull() {
        testList.add(new Spaceship("Life", 100, 0, 0));
        testList.add(new Spaceship("Leaf", 99, 0, 0));
        testList.add(new Spaceship("Lime", 98, 0,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        Spaceship result = commandCenter.getShipByName(testList, "Lie)");
        testList.clear();
        return result == null;
    }

    private boolean getAllShipsWithEnoughCargoSpace_returnList() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50, 0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 75);
        testList.clear();

        if (result.get(0).getCargoSpace() >= 75) {
            return true;
        }
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpace_returnEmptyList() {
        testList.add(new Spaceship("Life", 100, 101, 0));
        testList.add(new Spaceship("Leaf", 99, 51, 0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 125);
        testList.clear();
        return result.isEmpty();
    }

    private boolean getAllCivilianShips_returnList() {
        testList.add(new Spaceship("Life", 100, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50, 0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 0, 0,0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testList);
        testList.clear();
        if (result.get(0).getFirePower() == 0) {
            return true;
        }
        return false;
    }

    private boolean getAllCivilianShips_returnEmptyList() {
        testList.add(new Spaceship("Life", 101, 100, 0));
        testList.add(new Spaceship("Leaf", 99, 50, 0));
        testList.add(new Spaceship("Lime", 98, 25,0));
        testList.add(new Spaceship("Lol", 97, 0,0));

        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testList);
        testList.clear();
        return result.isEmpty();
    }
}
